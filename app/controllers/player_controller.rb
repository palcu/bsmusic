require 'youtube_it'

class PlayerController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_facebook_client, :only => [:first, :friendlist, :player]
  before_filter :get_youtube_client, :only => [:first, :search]
  
  
  def first
    songs = []
    if params[:id].nil?
      songs = @graph.fql_query("SELECT post_id, message, attachment.href FROM stream WHERE filter_key='app_2309869772' and strpos(attachment.href, 'youtube') > 0 limit 15")
      playlist = current_user.playlists.create(:name => "Facebook News Feed")
    else
      list = FacebookFriendlist.find_by_id(params[:id])
      members = @graph.get_connections(list.facebook_id, 'members')
      members.each do |member|
        songs += @graph.fql_query("SELECT post_id, message, attachment.href FROM stream WHERE filter_key='app_2309869772' and strpos(attachment.href, 'youtube') > 0 and source_id = '"+member['id']+"' limit 5")
      end
      playlist = current_user.playlists.create(:name => list.name)
    end

    # Parse songs for playlist
    songs.each do |song|
      song = playlist.add_song_from_facebook(song)
      song.name = @youtube_client.video_by(song.youtube_id).title
      song.save
    end

    redirect_to :controller => 'player', :action => 'player'
  end


  def friendlist
    lists = @graph.get_connections("me", "friendlists")
    current_user.compare_friendlists(lists)
    redirect_to :controller => 'player', :action => 'first'
  end

  def player
    @songs = current_user.playlists.last.songs
    @playlists = current_user.facebook_friendlists
    song_id = current_user.playlists.last.songs.first.facebook_id
    @song = @graph.get_object(song_id)
    @picture = @graph.get_picture(@song['from']['id'])
  end

  def owner
    id = params[:id]
    playlist_id = params[:playlist_id]

    playlist = current.user.playlists.find_by_id(playlist_id)
    song = playlist.find_by_id(id)
  end

  def search
    # c = @youtube_client.videos_by(:query => params[:query], :page => 1, :per_page =>3)
    # @youtube_id = parseYoutubeLink(c.videos[0].player_url)
    # @youtube_title = 'whatever' #c.videos[0].title
    respond_to do |format|
      format.js { render :json => 'palcuie' }
    end
  end

  private

  def get_facebook_client
    @graph = Koala::Facebook::API.new(current_user.facebook_token)
  end

  def get_youtube_client
    @youtube_client = client = YouTubeIt::Client.new(:dev_key => MySettings.youtube_key)
  end

  def parseYoutubeLink(link)
    if not link.nil?
      query_string = URI.parse(link).query
      if not query_string.nil?
        parameters = Hash[URI.decode_www_form(query_string)]
        if not parameters['v'].nil?
          return parameters['v']
        end
      end
      return false
    end
  end



end
