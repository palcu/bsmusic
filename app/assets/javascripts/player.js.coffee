# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  songs = $('#songs').data('songs')
  videos = []
  for song in songs
    x = 
      id: song.youtube_id
      title: song.name
    videos.push x
  config =
    playlist:
      title: 'Facebook News Feed',
      videos: videos
    chromeless: 0
    playlistHeight: 10
    showToolbar: 0
    autoStart: 1
    autoPlay: 1
    onEndPlaylist: () ->
      alert('the end')
  
  $(".player").player(config)
  return
