class Song < ActiveRecord::Base
  attr_accessible :facebook_id, :name, :playlist_id, :youtube_id
  validates_presence_of :facebook_id, :name, :playlist_id, :youtube_id
  belongs_to :playlist
end
