class FacebookFriendlist < ActiveRecord::Base
  attr_accessible :facebook_id, :name, :user_id

  validates_presence_of :facebook_id, :name, :user_id
  
  belongs_to :user
end
