class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :provider, :uid, :facebook_token
  
  has_many :playlists 
  has_many :facebook_friendlists

  def compare_friendlists(new_list)
    list = self.facebook_friendlists
    if list.count != new_list.count
      self.facebook_friendlists.delete_all
      new_list.each do |list|
        self.facebook_friendlists.create(:name => list["name"],
                                         :facebook_id => list["id"])
      end
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(
                           provider:auth.provider,
                           uid:auth.uid,
                           email:auth.info.email,
                           password: Devise.friendly_token[0,20],
                           facebook_token: auth.credentials.token
                           )
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end
