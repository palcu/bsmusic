class Playlist < ActiveRecord::Base
  attr_accessible :name, :user_id

  validates_presence_of :name, :user_id

  has_many :songs
  belongs_to :user

  def add_song_from_facebook(song)
    links = URI.extract(song["attachment"]["href"])

    youtube_link = nil
    links.each do |link|
      if link =~ /youtu/
        youtube_link = link
        break
      end
    end

    return false if youtube_link.nil?

    query_string = URI.parse(youtube_link).query

    return false if query_string.nil?

    parameters = Hash[URI.decode_www_form(query_string)]
    youtube_id = parameters['v']

    return false if youtube_id.nil?

    if self.songs.find_by_youtube_id(youtube_id).nil?
      return self.songs.create(:youtube_id => youtube_id,
                        :facebook_id => song["post_id"])
    end

  end
end
