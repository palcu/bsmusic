module PlayerHelper
  def parseYoutubeLink(link)
    if not link.nil?
      query_string = URI.parse(link).query
      if not query_string.nil?
        parameters = Hash[URI.decode_www_form(query_string)]
        if not parameters['v'].nil?
          return parameters['v']
        end
      end
      return false
    end
  end
end
