require 'test_helper'

class PlayerControllerTest < ActionController::TestCase
  test "should get first" do
    get :first
    assert_response :success
  end

  test "should get player" do
    get :player
    assert_response :success
  end

end
