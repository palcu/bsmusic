class AddUserIdToFacebookPlaylist < ActiveRecord::Migration
  def change
    add_column :facebook_playlists, :user_id, :integer
  end
end
