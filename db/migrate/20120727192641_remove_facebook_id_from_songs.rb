class RemoveFacebookIdFromSongs < ActiveRecord::Migration
  def up
    remove_column :songs, :facebook_id
  end

  def down
    add_column :songs, :facebook_id, :integer
  end
end
