class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :name
      t.integer :youtube_id
      t.integer :facebook_id
      t.integer :playlist_id

      t.timestamps
    end
  end
end
