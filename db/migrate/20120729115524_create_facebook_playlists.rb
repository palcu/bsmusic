class CreateFacebookPlaylists < ActiveRecord::Migration
  def change
    create_table :facebook_playlists do |t|
      t.string :name
      t.string :facebook_id

      t.timestamps
    end
  end
end
