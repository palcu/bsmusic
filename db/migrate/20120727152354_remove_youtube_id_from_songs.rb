class RemoveYoutubeIdFromSongs < ActiveRecord::Migration
  def up
    remove_column :songs, :youtube_id
  end

  def down
    add_column :songs, :youtube_id, :integer
  end
end
