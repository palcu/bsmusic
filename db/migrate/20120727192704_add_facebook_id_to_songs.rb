class AddFacebookIdToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :facebook_id, :string
  end
end
