class CreateFacebookFriendlists < ActiveRecord::Migration
  def change
    create_table :facebook_friendlists do |t|
      t.string :name
      t.string :facebook_id
      t.integer :user_id

      t.timestamps
    end
  end
end
